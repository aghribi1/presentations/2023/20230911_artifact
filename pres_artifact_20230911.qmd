---
title: <font size="6"><b>ART</b>ifical <b>I</b>ntelligence <b>F</b>or <b>A</b>ccelerators, user <b>C</b>ommunities and associated <b>T</b>echnologies</font>
subtitle: an INFRATEC-01-01-2024 project
format:
  clean-revealjs:
  # revealjs:
    self-contained: true
    touch: true
    controls: false
    logo: images/logo4.png
    # footer: A. GHRIBI - 2023/09/11
    menu: false
    slide-number: true
    show-slide-number: all
    embed-resources: true
    progress: true
    history: true
    # chalkboard: true
    mutliplex: true
    title-slide-attributes:
      data-background-image: images/logo4.png
      data-background-size: 50%
      data-background-position: top left
    mermaid:
      theme: neutral
revealjs-plugins:
  - pointer
author:
  - name: Adnan Ghribi
    orcid: 0000-0001-9634-4526
    email: adnan.ghribi@ganil.fr
    affiliations: GANIL / CNRS
  - name: On behalf of the coordination team
  #   orcid: 0000-0002-9048-8059
  #   email: kyle.butts@colorado.edu
  #   affiliations: UC Boulder
date: September 11, 2023
bibliography: refs.bib
# jupyter: python3
---

## Outline
- Introduction <br>
<font size="5">*Context, purpose and scope*</font>
- Building the consortium <br>
<font size="5">*Means and ways*</font>
- Project organisation <br>
<font size="5">*Partners, workpackages and planning*</font>

# Introduction {background-color="#40666e"}
::: footer

:::
Context, purpose, scope

## Context 
:::{.incremental}
- Artificial Intelligence applications for physics and society is revolutionizing the way we do and think our roles ;
- The acclerator community is catching up fast ;
- However, synergy is much needed for data as well as for methods and applications.
:::

## Purpose
:::{.incremental}
- We want to unlock the use of [**artificial intelligence**]{.alert} in particle and nuclear accelerators as well as in light/neutron sources ;
- We need something that would bring the missing piece of [**FAIRness**]{.alert} in data, methods and tools in ML for accelerators RI ;
- We project to push it to its edge in [**an integrated smart pilot accelerator**]{.alert} ;
- And we want to build a project that would be both [**challenging**]{.alert} and [**realistic**]{.alert} within a given time frame and budget.
:::

## Scope
- Name of the call : `INFRA-2023-TECH-01-01` ;
- Call opening : `2023/12/06` ;
- Call deadline : `2024/03/12` ;
- Budget : ~~`5` to~~ `10 M€`. 

## Aim
<!-- style="--col: #e64173" -->
::: {style="font-size: 50%;"}
The aim of this topic is to deliver <font size="6" color="MediumSeaGreen">innovative</font> scientific instrumentation, <font size="6" color="MediumSeaGreen">tools, methods and solutions</font> which advance the state-of-art of RIs in the EU and Associated Countries, and show <font size="6" color="MediumSeaGreen">transformative potential</font> in RIs operation. The related developments, which underpin the provision of <font size="6" color="MediumSeaGreen">improved and advanced services</font>, should lead research infrastructures to support new areas of research and/or a wider community of users, including industrial users. • Cutting-edge technologies will also enhance the potential of RIs to contribute addressing EU policy objectives and socio-economic challenges. • Proposals should ensure complementarity with actions funded under the previous 2022 call (topic HORIZON-INFRA-2022-TECH-01-01 in the 2021-2022 work programme), targeting different instrumentation, tools, methods and solutions. • Proposals should address the following aspects, as relevant: • Research and development of new scientific instrumentation, tools and methods for research infrastructures taking into due account resource efficiency (e.g. energy consumption) and environmental (including climate-related) impacts. This could also include the development of <font size="6" color="MediumSeaGreen">new, more sustainable and efficient methods of collecting data</font> and/or of <font size="6" color="MediumSeaGreen">providing access</font>, including remote and digital, as well as <font size="6" color="MediumSeaGreen">digitalisation of instrumentation, services and results</font>; their technology validation and <font size="6" color="MediumSeaGreen">prototyping training of RI staff</font> for the operation and use of these new solutions. When relevant, <font size="6" color="MediumSeaGreen">developing skills</font> on technical validation to industrial standards; the innovative potential for industrial exploitation of the solutions and/or for the <font size="6" color="MediumSeaGreen">benefits of the society</font>, including facilitating proof of concept for use by SMEs.
:::

## Expected outcomes

- [Enhanced scientific competitiveness]{.alert}. of RI ;
- [Enhanced RI capacities]{.alert}. to address research challenges EU policy priorities ;
- [Increased collaboration]{.alert}. of research infrastructures with universities, research organisation and industry ;
- [Increase of technological level of industries]{.alert}. through the co-development of advanced technologies of research infrastructures and creation of potential new markets ;
- [Integration of research infrastructures]{.alert}. into local, regional and global innovation systems and promotion of entrepreneurial culture.

## Fields of application
- Main field : [Accelerator physics and technologies and user communities]{.alert} ;
- Spans across different applications ;
  - [Particle physics]{.alert} ;
  - [Nuclear physics]{.alert} ;
  - [Light and Neutron sources]{.alert} ;
  - [Medical and industrial applications]{.alert} … ;
- Connects to [transverse applications]{.alert}.

# Building the consortium {background-color="#40666e"}
::: footer
:::
Means and ways

## Means
- AMORCE^[[Appui au Montage de projet de Recherche en Coordination Européenne](https://international.cnrs.fr/actualite/aap-amorce/)] + ANR MRSEI^[[Montage de Réseaux Scientifiques Européens ou Internationaux](https://anr.fr/fr/detail/call/montage-de-reseaux-scientifiques-europeens-ou-internationaux-mrsei-2023/)] $\rightarrow$ `40 k€` ;
- Dedicated European project engineers^[Jade Varin + Gwenaelle Auvray (CNRS)] $\rightarrow$ `CNRS` ;
- GANIL administrative support^[Sabrina Lecerf (+SVP)] ;
- ESF^[[European Science Foundation](https://www.esf.org/)] $\rightarrow$ `Project assessment and co-writing`.

## Ways
- Several preliminary meetings ; [[indico]{.button}](https://indico.cern.ch/category/17083/)
- Independent working groups actions for structuring the consortiumm and the call^[Virtual boards for brainstorming.] ;
- A two days workshop to conclude the first part actions; [[indico]{.button}](https://indico.cern.ch/event/1294919/)
- An ongoing overleaf white paper for use cases ;
- A shared IN2P3 box space ;
- Several distribution mailing lists ;
- A mattermost dedicated channel ;
- Representation at several conferences and committees.

# Project organisation {background-color="#40666e"}
Partners, workpackages and planning


## Academic Partners 
<!-- {background-image="images/countries.png" background-size="80%"} -->
:::: {.columns}

::: {.column width="20%"}
::: {style="font-size: 70%;"}
- 9 countries ;
- 18 partners^[direct + indirect.] ;
- 7 main beneficiaries.

:::{.callout-note }
Ongoing work
:::

:::
:::

::: {.column width="80%"}
![](images/countries.png){fig-align="center" width=100%}
:::
:::
<!-- {.absolute top=200 left=0 width="350" height="300"} -->

## Development flow

```{mermaid}
flowchart TD
    subgraph Transverse links
    t1(Management)
    t2(Communication and outreach)
    t3(Training and knowledge transfer)
    t4(Industrial applications and relations)
    t1-.-t2
    t2-.-t3
    t3-.-t4
    end
    subgraph Logical flow
    A(1. Collecting inputs from the community)
    B(2. Bringing FAIRness)
    C(3. Developing specific tools and methods)
    D(3.1 Simulations)
    E(3.2 Anomalies)
    F(3.3 Optimisation)
    G(4. Integrating into a pilot facility)
    A --> B
    B --> C
    C -.-> D
    C -.-> E
    C -.-> F
    D <-.-> E
    D <-.-> F
    E <-.-> F
    F --> G
    E --> G
    D --> G
    end
```


## Workpackages {auto-animate=true}
![](images/wp_2_4.png){fig-align="center" width=300%}

## Workpackages {auto-animate=true}
![](images/wp_2_4_1.png){fig-align="center" width=300%}

## Workpackages {auto-animate=true}
![](images/wp_2_4_2.png){fig-align="center" width=300%}

## Next steps
```{mermaid}
%%| "theme": "dark"
gantt
dateFormat  YYYY-MM-DD
%% title Adding GANTT diagram to mermaid
excludes weekdays 2014-01-10

section Consortium creation
Preliminary meetings          :done,    des1, 2023-03-01,2023-05-01
Working groups progress       :done,    des2, 2023-05-01,2023-07-01
Workshop           :milestone,    2023-07-15
Partners and WP definition :crit, active des4, 2023-07-02,2023-09-30

section First draft data
Tasks writing (1)  :         des5, 2023-08-01, 2023-09-30
Logbook filling (1)   :         des6, 2023-09-20, 2023-10-20           
Data collection         : des7, 2023-09-20, 2023-10-20
Financial data process.   :        des8, 2023-09-20, 2023-10-30
Organizational data process.  :         des9, 2023-09-20, 2023-10-30

section First draft proposal
WP writing (1)  :         des10, 2023-09-20, 30d
General sections (1)  :         des11, after des10, 15d
Document compilation (1)  :         des12, after des11, 15d

section Proposal revision
Adjustment of tasks :         des16, after des12, 30d
Ajustment of the logbook  :         des17, after des12, 30d
Data adjustment  :         des18, after des12, 30d
Financial data adjustment : des19, after des12, 30d
Organizational data adjustment  : des20, after des12, 30d

section Reading and review
Proofreading  :         des21, after des20, 30d
Project assessment  :         des22, after des20, 30d
1st review  :         des23, after des20, 30d
Adjustments :         des24, after des20, 30d
2nd review  :         des25, after des20, 30d

section Application processing
- :         des26, after des25, 2024-03-12
Submission : crit, milestone, 2024-03-01

%% Active task               :active,  des2, 2014-01-09, 3d
%% Future task               :         des3, after des2, 5d
%% Future task2               :         des4, after des3, 5d
```
##
::: {.r-fit-text}
Thank you
:::
<p style="text-align: center;">Questions ?</p>